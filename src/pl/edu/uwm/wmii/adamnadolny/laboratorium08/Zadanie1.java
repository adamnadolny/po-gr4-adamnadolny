package pl.edu.uwm.wmii.adamnadolny.laboratorium08;

import pl.imiajd.nadolny.Student2;
import pl.imiajd.nadolny.Pracownik;
import pl.imiajd.nadolny.Osoba2;
import java.time.LocalDate;


public class Zadanie1
{
    public static void main(String[] args)
    {
        Osoba2[] ludzie = new Osoba2[2];
        String[] imiona1 = {"Andrzej","Bartosz"};
        String[] imiona2 = {"Anna","Aleksandra"};
        ludzie[0] = new Pracownik("Kowalski",imiona1,LocalDate.of(1999,10,10), LocalDate.of(2009,11,11), true,2000);
        ludzie[1] = new Student2("Nowak", imiona2, LocalDate.of(1998,2,2),false, "Informatyka", 4.1);
        ludzie[1].setOpis2(5);
        for (Osoba2 p : ludzie) {
            System.out.println(p.getNazwisko()+" "+p.getImiona()+ ": " + p.getOpis()+"\nData urodzenia: "+p.getDataUrodzenia()+
                    " "+ p.getPlec()+" "+p.getOpis2());
        }
    }
}





