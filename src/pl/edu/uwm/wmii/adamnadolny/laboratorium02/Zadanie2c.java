package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[] tab, int n, int min, int max) {
        for(int i=0; i<n; i++)
        {
            tab[i] = losuj(min, max);
            System.out.println(tab[i]);
        }
    }
    public static void ileMaksymalnych (int tab[]){
        int ilosc=1;
        int max=tab[0];
        for (int i = 1; i < tab.length; i++) {
            if (tab[i] == max) {
                ilosc++;
            }
            if (tab[i] > max) {
                max = tab[i];
                ilosc=1;
            }
        }
        System.out.println("Liczba największa: " + max);
        System.out.println("Ilość wystąpień: " + ilosc);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        generuj(tablica, n, -999,999);
        ileMaksymalnych(tablica);
    }
}