package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        if(n<1)
        {
            System.out.println("Za mała");
        }
        if(n>100)
        {
            System.out.println("Za duża");
        }
        if(n>0 && n<101)
        {
            int[] tablica = new int[n];
            Random losuj = new Random();
            int parzyste=0;
            int nieparzyste=0;
            for (int i = 0; i < n; i++) {
                tablica[i] = losuj.nextInt(1999)-999;
                System.out.println(tablica[i]);
                if(tablica[i]%2==0)
                {
                    parzyste++;
                }
                else if(tablica[i]%2==1 || tablica[i]%2==-1)
                {
                    nieparzyste++;
                }
            }
            System.out.println("Ilość parzystych: " + parzyste);
            System.out.println("Ilość nieparzystych: " + nieparzyste);
        }
    }
}
