package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[] tab, int n, int min, int max) {
        for(int i=0; i<n; i++)
        {
            tab[i] = losuj(min, max);
            System.out.println(tab[i]);
        }
    }
    public static void ileNieparzystych (int tab[]){
        int ilosc=0;
        for(int elem: tab)
        {
          if(elem % 2==1 || elem%2==-1)
          {
              ilosc++;
          }
        }
       System.out.println("Ilość nieparzystych: " + ilosc);
    }
    public static void ileParzystych (int tab[]){
        int ilosc=0;
        for(int elem: tab)
        {
            if(elem % 2==0)
            {
                ilosc++;
            }
        }
        System.out.println("Ilość parzystych: " + ilosc);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        generuj(tablica, n, -999,999);
        ileNieparzystych(tablica);
        ileParzystych(tablica);
    }
}
