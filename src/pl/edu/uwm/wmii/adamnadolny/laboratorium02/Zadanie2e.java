package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2e {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[] tab, int n, int min, int max) {
        for(int i=0; i<n; i++)
        {
            tab[i] = losuj(min, max);
            System.out.println(tab[i]);
        }
    }
    public static void dlugoscMaksymalnegoCiaguDodatnich (int tab[]){
        int naj=0;
        int naj1=0;
        for (int elem: tab) {
            if(elem>0)
            {
                naj1++;
            }
            if(elem<0)
            {
                if(naj1>naj)
                {
                    naj=naj1;
                }
                naj1=0;
            }
        }
        if(naj1>naj)
        {
            naj=naj1;
        }
        System.out.println("Najdłuższy ciąg dodatnich: " + naj);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        generuj(tablica, n, -999,999);
        dlugoscMaksymalnegoCiaguDodatnich(tablica);
    }
}