package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        if(n<1)
        {
            System.out.println("Za mała");
        }
        if(n>100)
        {
            System.out.println("Za duża");
        }
        if(n>0 && n<101) {
            int[] tablica = new int[n];
            Random losuj = new Random();
            System.out.println("Tablica przed odwróceniem");
            for (int i = 0; i < n; i++) {
                tablica[i] = losuj.nextInt(1999)-999;
                System.out.println(tablica[i]);
            }
            Scanner scan1 = new Scanner(System.in);
            System.out.println("Podaj liczbe lewą");
            int lewy = scan1.nextInt();
            Scanner scan2 = new Scanner(System.in);
            System.out.println("Podaj liczbe prawą");
            int prawy = scan2.nextInt();
            int kosz=0;
            if(lewy>=1 && lewy<n && prawy>=1 && prawy<n && lewy<prawy) {
                System.out.println("Tablica po odwróceniu");
                for (int i = 0; i < n; i++) {
                    if (i <= lewy && lewy < prawy) {
                        kosz = tablica[lewy - 1];
                        tablica[lewy - 1] = tablica[prawy - 1];
                        tablica[prawy - 1] = kosz;
                        lewy++;
                        prawy--;
                    }
                    System.out.println(tablica[i]);
                }
            }
            else{
                System.out.println("Źle");
            }
        }
    }
}