package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[][] tab, int m, int n, int min, int max) {
        for(int i=0; i<m; i++)
        {
            for(int j=0; j<n; j++) {
                tab[i][j] = losuj(min, max);
                System.out.print(tab[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void iloczyn(int[][] tab, int[][] tab1, int[][] tab2, int m, int n) {
        for(int i=0; i<m; i++)
        {
            for(int j=0; j<m; j++) {
                for(int k = 0; k<n; k++) {
                    tab2[i][j] = tab2[i][j] + tab[i][k] * tab1[k][j];
                }
                System.out.print(tab2[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe m od 1 do 10");
        int m = scan.nextInt();
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Podaj liczbe n od 1 do 10");
        int n = scan1.nextInt();
        Scanner scan2 = new Scanner(System.in);
        int[][] tablica1 = new int[m][n];
        System.out.println("Podaj liczbe k od 1 do 10");
        int k = scan2.nextInt();
        int[][] tablica2 = new int[n][k];
        int[][] tablica3 = new int[m][k];
        System.out.println("Tablica A");
        generuj(tablica1, m, n, -10,10);
        System.out.println("Tablica B");
        generuj(tablica2, n, k, -10,10);
        System.out.println("Iloczyn macierzy A i B");
        iloczyn(tablica1,tablica2,tablica3,m,n);
    }
}