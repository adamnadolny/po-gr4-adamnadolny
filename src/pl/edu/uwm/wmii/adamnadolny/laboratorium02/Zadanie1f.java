package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        if(n<1)
        {
            System.out.println("Za mała");
        }
        if(n>100)
        {
            System.out.println("Za duża");
        }
        if(n>0 && n<101) {
            int[] tablica = new int[n];
            Random losuj = new Random();
            System.out.println("Tablica przed modyfikacją");
            for (int i = 0; i < n; i++) {
                tablica[i] = losuj.nextInt(1999)-999;
                System.out.println(tablica[i]);
            }
            System.out.println("Tablica po modyfikacji");
            for (int i = 0; i < n; i++) {
                if(tablica[i]<0)
                {
                    tablica[i]=-1;
                }
                if(tablica[i]>0)
                {
                    tablica[i]=1;
                }
                System.out.println(tablica[i]);
            }
        }
    }
}