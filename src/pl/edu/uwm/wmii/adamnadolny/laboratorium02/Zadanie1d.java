package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        if(n<1)
        {
            System.out.println("Za mała");
        }
        if(n>100)
        {
            System.out.println("Za duża");
        }
        if(n>0 && n<101)
        {
            int[] tablica = new int[n];
            Random losuj = new Random();
            int ujemne=0;
            int dodatnie=0;
            for (int i = 0; i < n; i++) {
                tablica[i] = losuj.nextInt(1999)-999;
                System.out.println(tablica[i]);
                if(tablica[i]<0)
                {
                    ujemne=ujemne+tablica[i];
                }
                if(tablica[i]>0)
                {
                    dodatnie=dodatnie+tablica[i];
                }
            }
            System.out.println("Suma dodatnich: " + dodatnie);
            System.out.println("Suma ujemnych: " + ujemne);
        }
    }
}