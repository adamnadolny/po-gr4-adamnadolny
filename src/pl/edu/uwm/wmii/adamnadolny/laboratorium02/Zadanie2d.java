package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2d {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[] tab, int n, int min, int max) {
        for(int i=0; i<n; i++)
        {
            tab[i] = losuj(min, max);
            System.out.println(tab[i]);
        }
    }
    public static void sumaDodatnich (int tab[]) {
        int suma=0;
        for (int elem : tab) {
            if (elem > 0) {
                suma = suma + elem;
            }
        }
        System.out.println("Suma dodatnich: " + suma);
    }
    public static void sumaUjemnych (int tab[]){
        int suma=0;
        for (int elem : tab) {
            if (elem < 0) {
                suma = suma + elem;
            }
        }
        System.out.println("Suma ujemnych: " + suma);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        generuj(tablica, n, -999,999);
        sumaDodatnich(tablica);
        sumaUjemnych(tablica);
    }
}