package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        if(n<1)
        {
            System.out.println("Za mała");
        }
        if(n>100)
        {
            System.out.println("Za duża");
        }
        if(n>0 && n<101)
        {
            int[] tablica = new int[n];
            Random losuj = new Random();
            for (int i = 0; i < n; i++) {
                tablica[i] = losuj.nextInt(1999)-999;
                System.out.println(tablica[i]);
            }
            int ilosc=1;
            int max=tablica[0];
            for (int i = 1; i < n; i++) {
                if (tablica[i] == max) {
                    ilosc++;
                }
                if (tablica[i] > max) {
                    max = tablica[i];
                    ilosc=1;
                }
            }
            System.out.println("Liczba największa: " + max);
            System.out.println("Ilość wystąpień: " + ilosc);
        }
    }
}