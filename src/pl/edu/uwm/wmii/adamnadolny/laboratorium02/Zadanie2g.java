package pl.edu.uwm.wmii.adamnadolny.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {
    public static int losuj(int min, int max) {
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }
    public static void generuj(int[] tab, int n, int min, int max) {
        for(int i=0; i<n; i++)
        {
            tab[i] = losuj(min, max);
            System.out.println(tab[i]);
        }
    }
    public static void odwrocFragment(int tab[], int lewy, int prawy){
        int kosz=0;
        if(lewy>=1 && lewy<tab.length && prawy>=1 && prawy<tab.length && lewy<prawy) {
            System.out.println("Po odwróceniu");
            for (int i = 0; i < tab.length; i++) {
                if (i <= lewy && lewy < prawy) {
                    kosz = tab[lewy - 1];
                    tab[lewy - 1] = tab[prawy - 1];
                    tab[prawy - 1] = kosz;
                    lewy++;
                    prawy--;
                }
                System.out.println(tab[i]);
            }
        }
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Podaj liczbe lewą");
        int lewy = scan1.nextInt();
        Scanner scan2 = new Scanner(System.in);
        System.out.println("Podaj liczbe prawą");
        int prawy = scan2.nextInt();
        generuj(tablica, n, -999,999);
        odwrocFragment(tablica, lewy, prawy);
    }
}