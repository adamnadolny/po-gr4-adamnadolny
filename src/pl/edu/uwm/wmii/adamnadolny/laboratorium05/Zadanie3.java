package pl.edu.uwm.wmii.adamnadolny.laboratorium05;

import java.util.ArrayList;

public class Zadanie3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> ar1, ArrayList<Integer> ar2){
        ArrayList<Integer> ar = new ArrayList<Integer>();
        ArrayList<Integer> arrr = new ArrayList<Integer>();
        ar.addAll(0, ar1);
        ar.addAll(ar1.size(), ar2);
        for(int i=0;i<ar1.size()+ar2.size();i++){
            int naj=ar.get(0);
         for(int j=1;j<ar.size();j++){
             if(ar.get(j)<naj){
                 naj=ar.get(j);
             }
         }
         arrr.add(i,naj);
         ar.remove(ar.indexOf(naj));
        }
        return arrr;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for(int i=0; i<15; i++){
            a.add(i, i+1);
        }
        ArrayList<Integer> b = new ArrayList<Integer>(8);
        for(int i=0; i<10; i++){
            b.add(i, b.size()+i+1);
        }
        System.out.println(a);
        System.out.println(b);
        System.out.println(mergeSorted(a,b));
    }
}
