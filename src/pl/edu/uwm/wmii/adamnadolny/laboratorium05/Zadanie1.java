package pl.edu.uwm.wmii.adamnadolny.laboratorium05;

import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> ar1, ArrayList<Integer> ar2){
        ArrayList<Integer> ar = new ArrayList<Integer>();
        ar.addAll(0, ar1);
        ar.addAll(ar1.size(), ar2);
        return ar;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for(int i=0; i<8; i++){
            a.add(i, i+1);
        }
        ArrayList<Integer> b = new ArrayList<Integer>(8);
        for(int i=0; i<8; i++){
            b.add(i, b.size()+i+1);
        }
        System.out.println(a);
        System.out.println(b);
        System.out.println(append(a,b));
    }
}
