package pl.edu.uwm.wmii.adamnadolny.laboratorium05;

import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> ar1){
        ArrayList<Integer> ar = new ArrayList<Integer>();
        for(int i=0;i<ar1.size();i++){
            ar.add(i, ar1.get(ar1.size()-i-1));
        }
        return ar;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for(int i=0; i<15; i++){
            a.add(i, i+1);
        }
        System.out.println(a);
        System.out.println(reversed(a));
    }
}
