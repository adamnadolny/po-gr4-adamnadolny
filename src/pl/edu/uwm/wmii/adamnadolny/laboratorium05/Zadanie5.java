package pl.edu.uwm.wmii.adamnadolny.laboratorium05;

import java.util.ArrayList;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> ar1){
        int b=ar1.size()-1;
        for(int i=0; i<ar1.size()/2; i++){
            int a=ar1.get(i);
            ar1.set(i, ar1.get(b));
            ar1.set(b, a);
            b--;
        }
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for(int i=0; i<15; i++){
            a.add(i, i+1);
        }
        System.out.println(a);
        reverse(a);
        System.out.println(a);
    }
}