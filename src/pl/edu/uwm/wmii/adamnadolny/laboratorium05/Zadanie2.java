package pl.edu.uwm.wmii.adamnadolny.laboratorium05;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> ar1, ArrayList<Integer> ar2){
        ArrayList<Integer> ar = new ArrayList<Integer>();
        ArrayList<Integer> m;
        ArrayList<Integer> d;
        m = ar1;
        d = ar2;
        if(ar2.size()<ar1.size()){
            m=ar2;
            d=ar1;
        }
        int j=0;
        int k=1;
        for(int i=0; i<m.size(); i++){
            ar.add(j, m.get(i));
            ar.add(k, d.get(i));
            j=j+2;
            k=k+2;
        }
        for(int i=0; i<d.size()-m.size(); i++){
            ar.add(m.size()*2+i, d.get(d.size()-(d.size()-m.size())+i));
        }
        return ar;
    }
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<Integer>(8);
        for(int i=0; i<13; i++){
            a.add(i, i+1);
        }
        ArrayList<Integer> b = new ArrayList<Integer>(8);
        for(int i=0; i<8; i++){
            b.add(i, b.size()+i+1);
        }
        System.out.println(a);
        System.out.println(b);
        System.out.println(merge(a,b));
    }
}

