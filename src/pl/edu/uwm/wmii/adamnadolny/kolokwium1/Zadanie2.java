package pl.edu.uwm.wmii.adamnadolny.kolokwium1;

public class Zadanie2 {
    public static String delete(String str, char c){
        char[] napis = new char[str.length()];
        StringBuffer s = new StringBuffer(str.length());
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)=='a') {
                napis[i]=c;
            }
            else {
                napis[i] = str.charAt(i);
            }
        }
        for (char napi : napis) {
            s.append(napi);
        }
        return s.toString();
        }
    public static void main(String[] args) {
             String a= "adam";
            char c= 'a';
            delete(a,c);
    }
}
