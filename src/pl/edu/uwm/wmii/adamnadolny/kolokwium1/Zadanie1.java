package pl.edu.uwm.wmii.adamnadolny.kolokwium1;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1 {
    public static void ilewiekszych (int[] tab){
        int ilosc=0;
        for(int elem: tab)
        {
            if(elem>5)
            {
                ilosc++;
            }
        }
        System.out.println("Ilosc wiekszych: " + ilosc);
    }
    public static void ilemniejszych (int[] tab){
        int ilosc=0;
        for(int elem: tab)
        {
            if(elem<(-5))
            {
                ilosc++;
            }
        }
        System.out.println("Ilosc mniejszych: " + ilosc);
    }
    public static void ilerownych (int[] tab){
        int ilosc=0;
        for(int elem: tab)
        {
            if(elem==(-5))
            {
                ilosc++;
            }
        }
        System.out.println("Ilosc rownych: " + ilosc);
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int n = scan.nextInt();
        int[] tablica = new int[n];
        Random losuj = new Random();
        for (int i = 0; i < n; i++) {
            tablica[i] = losuj.nextInt(20) - 9;
            System.out.println(tablica[i]);
        }
        ilewiekszych(tablica);
        ilemniejszych(tablica);
        ilerownych(tablica);
    }
}
