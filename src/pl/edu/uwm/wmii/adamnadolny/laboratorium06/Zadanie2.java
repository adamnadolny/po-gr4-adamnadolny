package pl.edu.uwm.wmii.adamnadolny.laboratorium06;

import java.util.Arrays;

public class Zadanie2 {
    public static void main(String[] args){
        IntegerSet s1=new IntegerSet();
        for(int i=0; i<100; i++){
        if(i<=8){
            s1.set[i]=true;
        }else{
            s1.set[i]=false;
        }
        }
        IntegerSet.writeSet(s1.set);
        IntegerSet s2=new IntegerSet();
        for(int i=0; i<100; i++){
            if(i<=11){
                s2.set[i]=true;
            }else{
                s2.set[i]=false;
            }
        }
        IntegerSet.writeSet(IntegerSet.union(s1.set,s2.set));
        IntegerSet.writeSet(IntegerSet.intersection(s1.set,s2.set));
        IntegerSet.writeSet(IntegerSet.insertElement(s1.set, 20));
        IntegerSet.writeSet(IntegerSet.deleteElement(s1.set, 2));
        System.out.println(s1.toString());

        System.out.println(s1.equals(s2));
        }
    }


   class IntegerSet{
    boolean[] set= new boolean[100];
    public static void writeSet(boolean[] set){
        for(int i=0; i<100; i++){
            if(set[i]==true){
                System.out.print(i+1 +" , ");
            }
        }
        System.out.println();
    }
    public static boolean[] union(boolean[] s1, boolean[] s2){
        boolean[] us = new boolean[100];
        for(int i=0; i<100; i++){
            if(s1[i]==false && s2[i]==false){
                us[i]=false;
            }
            else {
                us[i]=true;
            }
        }
        return us;
    }
       public static boolean[] intersection(boolean[] s1, boolean[] s2){
           boolean[] us = new boolean[100];
           for(int i=0; i<100; i++){
               if(s1[i]==true && s2[i]==true){
                   us[i]=true;
               }
               else {
                   us[i]=false;
               }
           }
           return us;
       }
       public static boolean[] insertElement(boolean[] s1, int a){
           boolean[] us = new boolean[100];
           for(int i=0; i<100; i++){
               if(s1[i]==true){
                   us[i]=true;
               }
               else {
                   us[i]=false;
               }
               us[a-1]=true;
           }
           return us;
       }

       @Override
       public String toString() {
           return "IntegerSet{" +
                   "set=" + Arrays.toString(set) +
                   '}';
       }

       @Override
       public boolean equals(Object o) {
           if (this == o) return true;
           if (o == null || getClass() != o.getClass()) return false;
           IntegerSet that = (IntegerSet) o;
           return Arrays.equals(set, that.set);
       }

       @Override
       public int hashCode() {
           return Arrays.hashCode(set);
       }

       public static boolean[] deleteElement(boolean[] s1, int a){
           boolean[] us = new boolean[100];
           for(int i=0; i<100; i++){
               if(s1[i]==true){
                   us[i]=true;
               }
               else {
                   us[i]=false;
               }
               us[a-1]=false;
           }
           return us;
       }
}

