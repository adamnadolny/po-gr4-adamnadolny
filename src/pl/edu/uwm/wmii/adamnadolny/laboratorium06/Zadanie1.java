package pl.edu.uwm.wmii.adamnadolny.laboratorium06;

public class Zadanie1 {
    public static void main(String[] args){
        RachunekBankowy[] osoba = new RachunekBankowy[2];
        osoba[0] = new RachunekBankowy("saver1",2000);
        osoba[1] = new RachunekBankowy("saver2",3000);
        System.out.println("Dla pierwszego miesiąca 4%");
        for(RachunekBankowy e: osoba){
            e.obliczMiesieczneOdsetki();
        }
        for(RachunekBankowy e: osoba){
            System.out.print("Nazwa = "+ e.nazwa());
            System.out.print("\tSaldo = "+ e.saldo()+"\n");
        }
        System.out.println("Dla następnego miesiąca 5%");
        RachunekBankowy.setRocznaStopaProcentowa();
        for(RachunekBankowy e: osoba){
            e.obliczMiesieczneOdsetki();
        }
        for(RachunekBankowy e: osoba){
            System.out.print("Nazwa = "+ e.nazwa());
            System.out.print("\tSaldo = "+ e.saldo()+"\n");
        }
    }
}
   class RachunekBankowy{
    public RachunekBankowy(String nazwa, double saldo){
        this.nazwa = nazwa;
        this.saldo = saldo;
    }
    public String nazwa(){
        return nazwa;
    }
    double saldo(){
        return saldo;
    }
    public void obliczMiesieczneOdsetki(){
        double odsetki=(saldo*rocznaStopaProcentowa)/12;
        saldo=saldo+odsetki;
    }
    public static void setRocznaStopaProcentowa() {
        rocznaStopaProcentowa = rocznaStopaProcentowa+0.01;
    }
    private double saldo;
    public String nazwa;
    public static double rocznaStopaProcentowa=0.04;
}
