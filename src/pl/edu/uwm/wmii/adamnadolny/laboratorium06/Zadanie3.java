package pl.edu.uwm.wmii.adamnadolny.laboratorium06;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Zadanie3 {

    public static void main(String[] args) {
        Pracownik[] personel = new Pracownik[3];

        personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        for (int i=0; i<3; i++) {
            if(i==0){
                System.out.print("nazwisko = " + personel[i].nazwisko() + "\tpobory = " + personel[i].pobory());
                System.out.printf("\tdataZatrudnienia = %tF\n", personel[i].dataZatrudnienia().minus(10, ChronoUnit.YEARS));
                i++;
            }
            System.out.print("nazwisko = " + personel[i].nazwisko() + "\tpobory = " + personel[i].pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", personel[i].dataZatrudnienia());
        }
        System.out.println();

    }
}

class Pracownik {

    public Pracownik(String nazwisko, double pobory, int year, int month, int day) {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        LocalDate calendar = LocalDate.of(year,month,day);

        dataZatrudnienia = calendar;
    }

    public String nazwisko() {
        return nazwisko;
    }

    public double pobory() {
        return pobory;
    }

    public LocalDate dataZatrudnienia() {

        return dataZatrudnienia;
    }

    public void zwiekszPobory(double procent) {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;
}