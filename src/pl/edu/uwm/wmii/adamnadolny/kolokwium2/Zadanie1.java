package pl.edu.uwm.wmii.adamnadolny.kolokwium2;

import pl.imiajd.nadolny.Autor;
import pl.imiajd.nadolny.Ksiazka;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Zadanie1 {
    public static void redukuj(LinkedList<String> books, int n){
        ListIterator<String> iter = books.listIterator();
        int i=0;
        while(iter.hasNext()){
            iter.next();
            i++;
            if(n==1){
                iter.remove();
            }
            if(iter.hasNext() && (i+1)%n==0){
                iter.next();
                i++;
                iter.remove();
            }
        }
    }

    public static void main(String[] args){
        ArrayList<Autor> autor = new ArrayList<>(4);
        autor.add(0,new Autor("Kowalczyk","adms@wp.pl",'M'));
        autor.add(1,new Autor("Zuzanna","zuza@wp.pl",'K'));
        autor.add(2,new Autor("Kowalczyk","bas@wp.pl",'K'));
        autor.add(3,new Autor("Bartosz","bartek@wp.pl",'M'));
        System.out.println(autor);
        autor.sort(Autor::compareTo);
        System.out.println(autor);

        System.out.println();

        ArrayList<Ksiazka> listaksiazek = new ArrayList<>(4);
        listaksiazek.add(0,new Ksiazka("Dziady",new Autor("Mickiewicz","mic@wp.pl",'M'),23.3));
        listaksiazek.add(1,new Ksiazka("Słoń",new Autor("Mickiewicz","mic@wp.pl",'M'),25.3));
        listaksiazek.add(2,new Ksiazka("Piraci",new Autor("Kowalczyk","kow@wp.pl",'M'),21.3));
        listaksiazek.add(3,new Ksiazka("Piraci",new Autor("Nowak","now@wp.pl",'K'),21.3));
        System.out.println(listaksiazek);
        listaksiazek.sort(Ksiazka::compareTo);
        System.out.println(listaksiazek);

        System.out.println();

        LinkedList<String> autor2 = new LinkedList<>();
        autor2.add(0,new Autor("Kowalczyk","adms@wp.pl",'M').toString());
        autor2.add(1,new Autor("Zuzanna","zuza@wp.pl",'K').toString());
        autor2.add(2,new Autor("Kowalczyk","bas@wp.pl",'K').toString());
        autor2.add(3,new Autor("Bartosz","bartek@wp.pl",'M').toString());
        System.out.println(autor2);
        redukuj(autor2,2);
        System.out.println(autor2);
    }
}



