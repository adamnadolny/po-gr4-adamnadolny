package pl.edu.uwm.wmii.adamnadolny.laboratorium11;

import java.time.LocalDate;
import java.util.Iterator;

public class Zadanie3 {
    public static void main(String[] args){
        Integer[] a = new Integer[3];
        a[0]=1;
        a[1]=2;
        a[2]=3;
        LocalDate[] b = new LocalDate[3];
        b[0]=LocalDate.of(1997,1,1);
        b[1]=LocalDate.of(1998,1,1);
        b[2]=LocalDate.of(1999,1,1);
        System.out.println(ArrayUtil.isSorted(a));
        System.out.println(ArrayUtil.isSorted(b));
    }
}

class ArrayUtil<T extends Comparable<T>>{
    public static <T extends Comparable<T>> boolean isSorted(T[] a){
        for(int i=0; i<a.length; i++){
            if(a[i].compareTo(a[i+1])==-1) return true;
            else break;
        }
        return false;
    }
}