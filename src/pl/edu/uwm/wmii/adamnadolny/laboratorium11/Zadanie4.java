package pl.edu.uwm.wmii.adamnadolny.laboratorium11;

import java.time.LocalDate;

public class Zadanie4 {
    public static void main(String[] args) {
        Integer[] a = new Integer[3];
        a[0] = 2;
        a[1] = 1;
        a[2] = 3;
        LocalDate[] b = new LocalDate[3];
        b[0] = LocalDate.of(1997, 1, 1);
        b[1] = LocalDate.of(1998, 1, 1);
        b[2] = LocalDate.of(1999, 1, 1);
        System.out.println(TabBin.binSearch(a,1));
        System.out.println(TabBin.binSearch(b,LocalDate.of(1999,1,1)));
    }
}

class TabBin<T extends Comparable<T>> {
    public static <T extends Comparable<T>> int binSearch(T[] tab, T szukanaLiczba) {
        int lewo = 0, prawo = tab.length - 1, srodek = 0;

        while (lewo <= prawo) {
            srodek = (lewo + prawo) / 2;
            if (tab[srodek] == szukanaLiczba)
                return srodek;
            else if (tab[srodek].compareTo(szukanaLiczba) < 0)
                lewo = srodek + 1;
            else
                prawo = srodek - 1;
        }

        return -1;
    }
}