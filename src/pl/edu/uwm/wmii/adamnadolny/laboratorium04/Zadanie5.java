package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie5 {

    public static void licz(double k, double p, int n){
        BigDecimal a = new BigDecimal(k);
        BigDecimal b = new BigDecimal(k);
        BigDecimal c = new BigDecimal(p);
        for(int i=1; i<=n; i++) {
            b=b.multiply(c);
            a=a.add(b);
            b=a;
        }
        System.out.println(a.setScale(2, BigDecimal.ROUND_HALF_EVEN));
    }

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj kapitał początkowy: ");
        double m = scan.nextDouble();
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Podaj stope procentową: ");
        double n = scan2.nextDouble();
        Scanner scan3 = new Scanner(System.in);
        System.out.print("Podaj ile lat: ");
        int o = scan3.nextInt();
        System.out.print("Wielkość kapitału po "+o+" latach wyniesie: ");
        licz(m, n, o);
    }
}