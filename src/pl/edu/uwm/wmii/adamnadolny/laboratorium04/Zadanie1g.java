package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.lang.String;
import java.util.Scanner;

public class Zadanie1g {
    public static String nice(String str){
        int a = (str.length()-1)/3;
        char[] napis = new char[str.length()+a];
        StringBuffer s = new StringBuffer(str.length()+a);
        int k=str.length()-1;
        int l = 1;
        for(int i=0; i<napis.length; i++){
            if(l%4==0){
                napis[i]='\'';
            }else{
                napis[i]=str.charAt(k);
                k--;
            }
            l++;
        }
        for (int j=napis.length-1; j>=0; j--) {
                s.append(napis[j]);
        }
        return s.toString();
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        String m = scan.nextLine();
        System.out.println(nice(m));
    }
}