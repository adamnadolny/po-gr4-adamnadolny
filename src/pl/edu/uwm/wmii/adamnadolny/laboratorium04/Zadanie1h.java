package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.util.Scanner;

public class Zadanie1h {
    public static String nice(String str, char x, int y){
        int a = (str.length()-1)/y;
        char[] napis = new char[str.length()+a];
        StringBuffer s = new StringBuffer(str.length()+a);
        int k=str.length()-1;
        int l = 1;
        for(int i=0; i<napis.length; i++){
            if(l%(y+1)==0){
                napis[i]=x;
            }else{
                napis[i]=str.charAt(k);
                k--;
            }
            l++;
        }
        for (int j=napis.length-1; j>=0; j--) {
            s.append(napis[j]);
        }
        return s.toString();
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        String m = scan.nextLine();
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Podaj separator: ");
        char n = scan1.next().charAt(0);
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Podaj co ile cyfr od prawej strony ma sie pojawiać separator: ");
        int o = scan2.nextInt();

        System.out.println(nice(m,n,o));
    }
}