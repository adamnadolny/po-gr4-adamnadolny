package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.util.Scanner;

public class Zadanie1b {

    public static int countSubstr(String str, String substr){
        int licz = 0;
        char znak1;
        char znak2;
        for(int i = 0; i<str.length()-substr.length()+1; i++) {
            int sprawdz = 1;
            for(int j = 0; j<substr.length(); j++) {
                znak1 = str.charAt(j+i);
                znak2 = substr.charAt(j);
                if (znak1 != znak2) {
                    sprawdz=0;
                }
            }
            if(sprawdz==1){
                licz++;
            }
        }
        return licz;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj pierwszy napis: ");
        String m = scan.nextLine();
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Podaj drugi napis: ");
        String n = scan1.next();
        System.out.println("Napis \""+n+"\" wystepuje "+countSubstr(m,n)+" razy w napisie \""+m+"\".");
    }
}
