package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.util.Scanner;

public class Zadanie1f {
    public static String change(String str){
        char[] napis = new char[str.length()];
        StringBuffer s = new StringBuffer(str.length());
        for(int i=0; i<str.length(); i++){
            napis[i]=str.charAt(i);
        }
        char[] napis2 = new char[str.length()];
        for(int j=0; j<str.length(); j++){
            if(Character.isUpperCase(napis[j])){
                napis2[j]=Character.toLowerCase(napis[j]);
            }
            else{
                napis2[j]=Character.toUpperCase(napis[j]);
            }
        }
        for (char c : napis2) {
            s.append(c);
        }
        return s.toString();
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String m = scan.nextLine();
        System.out.println(change(m));
    }
}