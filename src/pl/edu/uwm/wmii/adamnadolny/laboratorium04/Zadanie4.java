package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4 {

    public static void szachownica(int n){
        BigInteger a = new BigInteger("0");
            for(int i=0; i<n*n; i++) {
                BigInteger b = new BigInteger("2");
                b=b.pow(i);
                a=a.add(b);
            }
        System.out.println(a);
    }

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        int m = scan.nextInt();
        System.out.print("Ilość ziarenek maku w szachownicy "+m+"x"+m+" to: ");
        szachownica(m);
    }
}