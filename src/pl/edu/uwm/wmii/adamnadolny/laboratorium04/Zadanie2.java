package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2 {

    public static int countChar(File a, char c) throws FileNotFoundException{
        int licz = 0;
        char znak;
        Scanner scan = new Scanner(a);
        while(scan.hasNextLine()) {
            String napis = scan.nextLine();
            for (int i = 0; i < napis.length(); i++) {
                znak = napis.charAt(i);
                if (znak == c) {
                    licz++;
                }
            }
        }
        return licz;
    }

    public static void main(String[] args) throws FileNotFoundException{
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj nazwe pliku: ");
        String m = scan.nextLine();
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Podaj litere: ");
        char n = scan1.next().charAt(0);
        System.out.println("W pliku \""+m+"\" litera \""+n+"\" występuje "+countChar(new File(m),n)+" razy.");
    }
}