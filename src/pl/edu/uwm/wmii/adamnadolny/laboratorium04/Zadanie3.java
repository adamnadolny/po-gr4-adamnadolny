package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie3 {

    public static int countSubstr(File a, String substr) throws FileNotFoundException{
        int licz = 0;
        char znak1;
        char znak2;
        Scanner scan = new Scanner(a);
        while(scan.hasNextLine()) {
            String napis = scan.nextLine();
            for (int i = 0; i < napis.length() - substr.length() + 1; i++) {
                int sprawdz = 1;
                for (int j = 0; j < substr.length(); j++) {
                    znak1 = napis.charAt(j + i);
                    znak2 = substr.charAt(j);
                    if (znak1 != znak2) {
                        sprawdz = 0;
                    }
                }
                if (sprawdz == 1) {
                    licz++;
                }
            }
        }
        return licz;
    }

    public static void main(String[] args) throws FileNotFoundException{
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj nazwe pliku: ");
        String m = scan.nextLine();
        Scanner scan2 = new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String n = scan2.nextLine();
        System.out.println("Napis \""+n+"\" wystepuje "+countSubstr(new File(m),n)+" razy w pliku \""+m+"\".");
    }
}
