package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.util.Scanner;

public class Zadanie1a {

    public static int countChar(String str, char c){
      int licz = 0;
      char znak;
      for(int i = 0; i<str.length(); i++) {
          znak = str.charAt(i);
          if(znak==c){
            licz++;
          }
      }
      return licz;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String napis = scan.next();
        Scanner scan1 = new Scanner(System.in);
        System.out.print("Podaj litere: ");
        char m = scan.next().charAt(0);
        System.out.println("Litera \""+ m +"\" wystąpiła "+countChar(napis,m)+" razy w napisie \""+napis+"\".");
    }
}
