package pl.edu.uwm.wmii.adamnadolny.laboratorium04;
import java.util.Scanner;


public class Zadanie1c {

    public static String middle(String str){
        if(str.length()%2==0){
           return str.substring(str.length()/2-1,(str.length()/2)+1);
        }
        if(str.length()%2==1){
            return str.substring(str.length()/2,(str.length()/2)+1);
        }
        return str;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj napis: ");
        String m = scan.nextLine();
        System.out.println(middle(m));
    }
}