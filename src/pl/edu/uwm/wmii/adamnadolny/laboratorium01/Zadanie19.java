package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie19 {
    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = 7;
        sequence[8] = 4;
        sequence[9] = 16;
        int wynik=0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i]>0){
                wynik = wynik + (sequence[i]*2);
            }
        }
        System.out.println(wynik);
    }
}
