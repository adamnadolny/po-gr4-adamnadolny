package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie8 {
    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -7;
        sequence[1] = -5;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 5;
        sequence[7] = 7;
        sequence[8] = 9;
        sequence[9] = 11;
        int result = 0;
        for (int i = 0; i < 10; i++) {
            if(i%2==0) {
                result = result + sequence[i];
            }
            else {
                result = result - sequence[i];
            }
        }
        System.out.println(result);
    }
}
