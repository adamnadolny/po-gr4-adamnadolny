package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie21 {
    public static void main(String[] args) {
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 17;
        sequence[6] = -61;
        sequence[7] = 0;
        sequence[8] = 4;
        sequence[9] = 16;
        int min = sequence[0];
        int max = sequence[0];
        for (int i = 1; i < 10; i++) {
            if (sequence[i] > max) {
                max = sequence[i];
            }
        }
        for (int i = 1; i < 10; i++) {
            if (sequence[i] < min) {
                min = sequence[i];
            }
        }
        System.out.println("Liczba najwieksza: " + max);
        System.out.println("Liczba najmniejsza: " + min);
    }
}
