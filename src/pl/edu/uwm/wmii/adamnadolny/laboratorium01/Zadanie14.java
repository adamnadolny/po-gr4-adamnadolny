package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie14 {
    public static void main(String[] args){
        double sequence[];
        sequence = new double[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = 7;
        sequence[8] = 4;
        sequence[9] = 16;
        int a=0;
        double b=0;
        for (int i = 1; i < 9; i++) {
           b=(sequence[i-1]+sequence[i+1])/2;
           if(sequence[i]<b){
              a++;
               System.out.println(sequence[i]);
           }
        }
        System.out.println("Ilość: "+ a);
    }
}
