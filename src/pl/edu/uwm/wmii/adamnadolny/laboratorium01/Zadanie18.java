package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie18 {
    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = 7;
        sequence[8] = 4;
        sequence[9] = 16;
        int a=0;
        for (int i = 0; i < 10; i++) {
            if(Math.abs(sequence[i])<Math.pow(i+1, 2)){
                a++;
                System.out.println(sequence[i]);
            }
        }
        System.out.println("Ilość: "+ a);
    }
}
