package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

public class Zadanie20 {
    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = -1;
        sequence[4] = 1;
        sequence[5] = 0;
        sequence[6] = 6;
        sequence[7] = 0;
        sequence[8] = 4;
        sequence[9] = 16;
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i]<0){
                ujemne++;
            }
            if(sequence[i]>0){
                dodatnie++;
            }
            if(sequence[i]==0)
            {
                zera++;
            }
        }
        System.out.println("Ilość dodatnich: "+ dodatnie);
        System.out.println("Ilość ujemnych: "+ ujemne);
        System.out.println("Ilość zer: "+ zera);
    }
}
