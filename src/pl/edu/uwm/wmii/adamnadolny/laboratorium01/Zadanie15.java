package pl.edu.uwm.wmii.adamnadolny.laboratorium01;

import java.lang.Math;

public class Zadanie15 {
    static double silnia(int n)
    {
        if (n == 0)
            return 1;

        return n*silnia(n-1);
    }
    public static void main(String[] args){
        int sequence[];
        sequence = new int[10];
        sequence[0] = -16;
        sequence[1] = -10;
        sequence[2] = -3;
        sequence[3] = 17;
        sequence[4] = 34;
        sequence[5] = 3;
        sequence[6] = 6;
        sequence[7] = 7;
        sequence[8] = 4;
        sequence[9] = 16;
        int a=0;
        for (int i = 0; i < 10; i++) {
            if(sequence[i]>Math.pow(2, i+1) && sequence[i]<silnia(i+1)){
                a++;
                System.out.println(sequence[i]);
            }
        }
        System.out.println("Ilość: "+ a);
    }
}
