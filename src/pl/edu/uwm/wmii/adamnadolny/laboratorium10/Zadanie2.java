package pl.edu.uwm.wmii.adamnadolny.laboratorium10;

import pl.imiajd.nadolny.Student3;

import java.time.LocalDate;
import java.util.ArrayList;


public class Zadanie2 {
    public static void main(String[] args) {
        ArrayList<Student3> Grupa = new ArrayList<>(5);
        Grupa.add(0, new Student3("Zalewski", LocalDate.of(1999, 2, 4),4.0));
        Grupa.add(1, new Student3("Zalewski", LocalDate.of(1999, 1, 2),5.0));
        Grupa.add(2, new Student3("Nadolny", LocalDate.of(1999, 1, 29),3.5));
        Grupa.add(3, new Student3("Mickiewicz", LocalDate.of(1999, 1, 1),4.5));
        Grupa.add(4, new Student3("Mickiewicz", LocalDate.of(1999, 1, 1), 3.0));
        System.out.println(Grupa);
        Grupa.sort(null);
        System.out.println(Grupa);
    }
}
