package pl.edu.uwm.wmii.adamnadolny.laboratorium10;

import pl.imiajd.nadolny.Osoba3;

import java.time.LocalDate;
import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String[] args) {
        ArrayList<Osoba3> Grupa = new ArrayList<>(5);
        Grupa.add(0, new Osoba3("Zalewski", LocalDate.of(1999, 2, 4)));
        Grupa.add(1, new Osoba3("Zalewski", LocalDate.of(1999, 1, 2)));
        Grupa.add(2, new Osoba3("Nadolny", LocalDate.of(1999, 1, 29)));
        Grupa.add(3, new Osoba3("Mickiewicz", LocalDate.of(1999, 1, 1)));
        Grupa.add(4, new Osoba3("Mickiewicz", LocalDate.of(1999, 1, 1)));
        System.out.println(Grupa);
        Grupa.sort(null);
        System.out.println(Grupa);
    }
}

