package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

import java.util.LinkedList;

public class Zadanie4 {
    public static <T extends Comparable<T>> void odwroc(LinkedList<T> lista)
    {
        for (int i = 0; i < lista.size() / 2; i++) {
            T a = lista.get(i);
            lista.set(i, lista.get(lista.size() - i - 1));
            lista.set(lista.size() - i - 1, a);
        }
    }
    public static void main(String[] args){
        LinkedList<Integer> a = new LinkedList<>();
        a.add(1);
        a.add(3);
        a.add(5);
        a.add(3);
        a.add(7);
        a.add(9);
        a.add(1);
        a.add(3);
        a.add(2);
        a.add(4);
        odwroc(a);
        System.out.println(a);

    }
}
