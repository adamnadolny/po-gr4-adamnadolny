package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

import java.util.LinkedList;

public class Zadanie3 {
        public static void odwroc(LinkedList<String> lista)
        {
            for (int i = 0; i < lista.size() / 2; i++) {
                String a = lista.get(i);
                lista.set(i, lista.get(lista.size() - i - 1));
                lista.set(lista.size() - i - 1, a);
            }
        }
    public static void main(String[] args){
        LinkedList<String> a = new LinkedList<>();
        a.add("Adam");
        a.add("Andrzej");
        a.add("Albert");
        a.add("Aleksander");
        a.add("Bartosz");
        a.add("Krzysztof");
        a.add("Damian");
        a.add("Anna");
        a.add("Aleksandra");
        a.add("Katarzyna");
        odwroc(a);
        System.out.println(a);

    }
}
