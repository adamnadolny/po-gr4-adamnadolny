package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

public class Zadanie7 {
    public static void sito(int n){
        boolean[] a=new boolean[n];
        int i,j;
        for(i=0; i<n; i++){
            a[i]=true;
        }
        for(i=2; i<n;i++){
            if(a[i]){
                System.out.print(i+"\t");
                for(j=2*i; j<n; j=j+i){
                    a[j]=false;
                }
            }
        }
    }
    public static void main(String[] args) {
        sito(100);
    }
}
