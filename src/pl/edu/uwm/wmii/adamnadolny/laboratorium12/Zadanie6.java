package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

import java.util.Stack;

public class Zadanie6 {
    public static void podziel(int n){
        Stack<Integer> stak= new Stack<>();
        while(n>0){
            stak.push(n%10);
            n=n/10;
        }
        while(!stak.isEmpty()){
            System.out.print(stak.pop()+" ");
        }
    }
    public static void main(String[] args) {
        podziel(2015);
    }
}
