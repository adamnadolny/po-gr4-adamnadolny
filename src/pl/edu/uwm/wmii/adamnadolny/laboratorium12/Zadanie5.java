package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

import java.util.Stack;

public class Zadanie5 {
    public static void odwroc(String b){
        char[] napis = new char[b.length()];
        StringBuffer s = new StringBuffer(b.length());
        int j=0;
        int p=0;
        Stack<String> stak= new Stack<>();
        for(int i=0; i<b.length(); i++){
            napis[j]=b.charAt(i);
            if(napis[j]=='.'){
                napis[j]=' ';
            }
            j++;
            if(b.charAt(i)==' '){
                for(int l = 0; l<j; l++){
                    s.append(Character.toLowerCase(napis[l]));
                }
                if(p==0){
                    s.deleteCharAt(s.length()-1);
                    p=1;
                    s.append(". ");
                }
                stak.push(s.toString());
                j=0;
                s = new StringBuffer();
            }
            if(b.charAt(i)=='.'){
                p=0;
                s.append(Character.toUpperCase(napis[0]));
                for(int l = 1; l<j; l++){
                    s.append(napis[l]);
                }
                stak.push(s.toString());
                j=0;
                s = new StringBuffer();
                while (!stak.isEmpty()){
                    System.out.print(stak.pop());
                }
            }
        }
    }

    public static void main(String[] args) {
        String a= "Ala ma kota. Jej kot lubi myszy.";
        odwroc(a);
    }
}
