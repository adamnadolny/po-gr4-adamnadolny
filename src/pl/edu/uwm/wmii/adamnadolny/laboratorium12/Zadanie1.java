package pl.edu.uwm.wmii.adamnadolny.laboratorium12;

import java.util.LinkedList;

import java.util.ListIterator;

public class Zadanie1 {
    public static void redukuj(LinkedList<String> pracownicy, int n){
        ListIterator<String> iter = pracownicy.listIterator();
        int i=0;
        while(iter.hasNext()){
            iter.next();
            i++;
            if(n==1){
                iter.remove();
            }
            if(iter.hasNext() && (i+1)%n==0){
                iter.next();
                i++;
                iter.remove();
            }
        }
    }

    public static void main(String[] args){
        LinkedList<String> a = new LinkedList<>();
        a.add("Adam");
        a.add("Andrzej");
        a.add("Albert");
        a.add("Aleksander");
        a.add("Bartosz");
        a.add("Krzysztof");
        a.add("Damian");
        a.add("Anna");
        a.add("Aleksandra");
        a.add("Katarzyna");
        redukuj(a,3);
        System.out.println(a);

    }
}
