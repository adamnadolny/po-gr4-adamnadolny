package pl.edu.uwm.wmii.adamnadolny.laboratorium13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie2 {
    public static void main(String[] args) {
        Map<String, String> studenci = new TreeMap<>();
        studenci.put("Adam", "db+");
        studenci.put("Andrzej", "db");
        studenci.put("Anna", "bdb");
        int b;
        do{
        Scanner scan = new Scanner(System.in);
        System.out.println("Wybierz opcje: ");
        System.out.println("\n1. Dodaj studenta");
        System.out.println("2. Usuń studenta");
        System.out.println("3. Zmień ocene");
        System.out.println("4. Wyświetl liste studentów");
        System.out.println("5. Zakończ");
        b = scan.nextInt();

        if(b==1){
            Scanner scan4 = new Scanner(System.in);
            System.out.println("Podaj imie");
            String e = scan4.next();
            Scanner scan5 = new Scanner(System.in);
            System.out.println("Podaj ocene");
            String f = scan5.next();

            studenci.put(e,f);
        }

        if(b==2){
            Scanner scan3 = new Scanner(System.in);
            System.out.println("Podaj imie");
            String d = scan3.next();

            studenci.remove(d);
        }

        if(b==3){
            Scanner scan1 = new Scanner(System.in);
            System.out.println("Podaj imie");
            String a = scan1.next();
            Scanner scan2 = new Scanner(System.in);
            System.out.println("Podaj ocene");
            String c = scan2.next();

            studenci.replace(a,c);
        }

        if (b == 4) {
            for (Map.Entry<String, String> wpis : studenci.entrySet()) {
                String klucz = wpis.getKey();
                String wartosc = wpis.getValue();
                System.out.println(klucz + ": " + wartosc);
            }
            }
        }
        while (b!=5);
    }
}

