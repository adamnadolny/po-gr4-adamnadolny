package pl.edu.uwm.wmii.adamnadolny.laboratorium13;

import java.util.PriorityQueue;
import java.util.Scanner;
public class Zadanie1 {
    public static void addTask(PriorityQueue<Zadanie> list){
        String n;
        do{
            Scanner scan = new Scanner(System.in);
            System.out.println("Wpisz polecenie: ");
            System.out.println("\n1. dodaj priorytet opis");
            System.out.println("2. następny");
            System.out.println("3. zakończ");
            n = scan.nextLine();
            if (n.equals("dodaj priorytet opis")) {
                Scanner scan1 = new Scanner(System.in);
                System.out.print("Podaj priorytet zadania: ");
                int a = scan1.nextInt();
                Scanner scan2 = new Scanner(System.in);
                System.out.print("Podaj opis zadania: ");
                String b = scan2.next();
                list.add(new Zadanie(a, b));
            }
            if (n.equals("następne")) {
                list.remove();
            }
        }while(!n.equals("zakończ"));
    }

    public static void main(String[] args) {
        PriorityQueue<Zadanie> lista = new PriorityQueue<>();
            lista.add(new Zadanie(3,"zadanie3"));
            lista.add(new Zadanie(1,"zadanie1"));
            lista.add(new Zadanie(2,"zadanie2"));
            addTask(lista);
            for(Zadanie z: lista){
               System.out.println(z.opis);
            }
    }
}

class Zadanie implements Comparable<Zadanie>{
    Zadanie(int priorytet, String opis){
        this.priorytet=priorytet;
        this.opis=opis;
    }
    public int priorytet;
    public String opis;

    public int compareTo(Zadanie o) {
        return 0;
    }
}

