package pl.edu.uwm.wmii.adamnadolny.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        String name = "Co to za życie bywa w młodości?\n" +
                      "Nie czujesz serca, wątroby, kości,\n" +
                      "Śpisz jak zabity, popijasz gładko\n" +
                      "I nawet głowa boli cię rzadko.\n\n" +
                      "Dopiero człeku twój wiek dojrzały\n" +
                      "Odsłania życia urok wspaniały,\n" +
                      "Gdy łyk powietrza z wysiłkiem łapiesz,\n" +
                      "Rwie cię w kolanach, na schodach sapiesz,\n\n" +
                      "Serce jak głupie szybko ci bije,\n" +
                      "Lecz w każdej chwili czujesz, że ŻYJESZ!\n\n" +
                      "Więc nie narzekaj z byle powodu,\n" +
                      "Masz teraz wszystko, czego za młodu\n\n" +
                      "Nie doświadczyłeś… Ale DOŻYŁEŚ!";
        System.out.println(name);
    }
}
