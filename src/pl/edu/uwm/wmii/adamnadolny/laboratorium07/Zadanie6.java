package pl.edu.uwm.wmii.adamnadolny.laboratorium07;
import pl.imiajd.nadolny.BetterRectangle;
public class Zadanie6 {
    public static void main(String[] args){
        BetterRectangle a = new BetterRectangle();
        a.setSize(2,3);
        a.setLocation(2,2);
        System.out.println(a.getArea());
        System.out.println(a.getPerimeter());
    }
}