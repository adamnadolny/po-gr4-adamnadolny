package pl.edu.uwm.wmii.adamnadolny.laboratorium07;
import pl.imiajd.nadolny.Student;
import pl.imiajd.nadolny.Nauczyciel;

public class Zadanie4 {
    public static void main(String[] args){
        Student adam = new Student("Nadolny",1999,"Informatyczny");
        Nauczyciel jan = new Nauczyciel("Kowalski",1980,2560);
        System.out.println(adam.getNazwisko()+" "+adam.getRokurodzenia()+" "+adam.getKierunek());
        System.out.println(jan.getNazwisko()+" "+jan.getRokurodzenia()+" "+jan.getPensja());
        System.out.println(adam.toString());
        System.out.println(jan.toString());
    }
}