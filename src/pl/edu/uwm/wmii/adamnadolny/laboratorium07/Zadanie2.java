package pl.edu.uwm.wmii.adamnadolny.laboratorium07;
import pl.imiajd.nadolny.Adres;

public class Zadanie2 {
    public static void main(String[] args){
        Adres[] dom=new Adres[2];
        dom[0] = new Adres("Mickiewicza","20","3","Leg Staroscinski","07-399");
        dom[1] = new Adres("Micki","23","7","Leg","07-400");
        dom[0].pokaz();
        Adres.przed(dom[0],dom[1]);
    }
}
