package pl.imiajd.nadolny;

import java.time.LocalDate;
import java.util.Objects;

interface IcomClon extends Comparable, Cloneable{}
public class Osoba3 implements IcomClon{
    public Osoba3(String nazwisko, LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }

    @Override
    public String toString() {
        return "Osoba[" +
                "nazwisko='" + nazwisko + '\'' + String.format(" data urodzenia: %tF ", dataUrodzenia)+
                ']';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba3 osoba3 = (Osoba3) o;
        return Objects.equals(nazwisko, osoba3.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba3.dataUrodzenia);
    }
    @Override
    public int compareTo(Object o) {
        if(nazwisko != ((Osoba3) o).nazwisko){
            return -1;
        }
        if(dataUrodzenia != ((Osoba3) o).dataUrodzenia){
            return -1;
        }
        return 0;
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}