package pl.imiajd.nadolny;

import java.time.LocalDate;
import java.util.Arrays;

public abstract class Osoba2
{
    public Osoba2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();
    public abstract String getOpis2();
    public abstract void setOpis2(double a);
    public String getImiona(){
        return Arrays.toString(imiona);
    }

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public String getPlec(){
        if(plec==true){
            return "mezczyzna";
        }
        return "kobieta";
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

}
