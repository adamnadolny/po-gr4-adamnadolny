package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Flet extends Instrumenty
{
    public Flet(String producent, LocalDate rokProdukcji, String dzwiek){
        super(producent, rokProdukcji);
        this.dzwiek=dzwiek;
    }
    public String getDzwiek(){
        return dzwiek;
    }
    private String dzwiek;
}
