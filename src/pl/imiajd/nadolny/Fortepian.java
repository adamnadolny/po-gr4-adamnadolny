package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Fortepian extends Instrumenty
{
    public Fortepian(String producent, LocalDate rokProdukcji, String dzwiek){
        super(producent, rokProdukcji);
        this.dzwiek=dzwiek;
    }
    public String getDzwiek(){
        return dzwiek;
    }
    private String dzwiek;
}
