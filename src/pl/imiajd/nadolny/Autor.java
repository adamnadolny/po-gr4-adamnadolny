package pl.imiajd.nadolny;

interface IcomClone extends Comparable, Cloneable{}
public class Autor implements IcomClone{
    public Autor(String nazwa, String email, char plec){
        this.nazwa=nazwa;
        this.email=email;
        this.plec=plec;
    }
    @Override
    public String toString() {
        return "Autor[" +
                "nazwa=" + nazwa + ",email=" + email+",plec="+ plec+
                "]\n";

    }

    public int compareTo(Object o) {
        if (nazwa.charAt(0) > ((Autor)o).nazwa.charAt(0)) {
            return 1;
        } else if (nazwa.charAt(0) < ((Autor)o).nazwa.charAt(0)) {
            return -1;
        } else {
            if (plec < ((Autor)o).plec) {
                return -1;
            } else if (plec > ((Autor)o).plec) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public String getNazwa(){
        return nazwa;
    }
    public String getEmail(){
        return email;
    }
    public char getPlec(){
        return plec;
    }
    public void setNazwa(String a){
        nazwa=a;
    }
    public void setEmail(String b){
        nazwa=b;
    }
    public void setPlec(String c){
        nazwa=c;
    }

    private String nazwa;
    private String email;
    private char plec;
}
