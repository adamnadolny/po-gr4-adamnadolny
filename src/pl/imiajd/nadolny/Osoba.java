package pl.imiajd.nadolny;

public class Osoba{
    public Osoba(String nazwisko, int rokurodzenia){
        this.nazwisko=nazwisko;
        this.rokurodzenia=rokurodzenia;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "nazwisko='" + nazwisko + '\'' +
                ", rokurodzenia=" + rokurodzenia +
                '}';
    }

    public String getNazwisko(){
        return nazwisko;
    }
    public int getRokurodzenia(){
        return rokurodzenia;
    }
    private String nazwisko;
    private int rokurodzenia;
}
