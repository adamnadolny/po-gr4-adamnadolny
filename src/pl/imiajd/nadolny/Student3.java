package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Student3 extends Osoba3 implements IcomClon {
    public Student3(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    @Override
    public String toString() {
        return super.toString() + "srednia: " + sredniaOcen;
    }

    @Override
    public int compareTo(Object o) {
        return super.compareTo(o);
    }

    private double sredniaOcen;
}
