package pl.imiajd.nadolny;

public class Ksiazka implements IcomClone{
    public Ksiazka(String tytul, Autor autor, double cena){
        this.tytul=tytul;
        this.autor=autor;
        this.cena=cena;
    }
    @Override
    public String toString() {
        return "Książka[" +
                "tytuł=" + tytul + ",autor=" + super.toString()+",cena="+ cena+
                ']';
    }
    public int compareTo(Object o) {
        if (autor == ((Ksiazka)o).autor) {
            return 1;
        } else if (autor != ((Ksiazka)o).autor) {
            return -1;
        } else {
            if (tytul.charAt(0) < ((Ksiazka)o).tytul.charAt(0)) {
                return -1;
            } else if (tytul.charAt(0) > ((Ksiazka)o).tytul.charAt(0)) {
                return 1;
            } else {
                if (cena < ((Ksiazka)o).cena) {
                    return -1;
                } else if (cena > ((Ksiazka)o).cena) {
                    return 1;
                }
                else{
                    return 0;
                }
            }
        }
    }
    private String tytul;
    private Autor autor;
    private double cena;
}
