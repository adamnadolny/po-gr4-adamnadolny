package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Skrzypce extends Instrumenty
{
    public Skrzypce(String producent, LocalDate rokProdukcji, String dzwiek){
        super(producent, rokProdukcji);
        this.dzwiek=dzwiek;
    }
    public String getDzwiek(){
        return dzwiek;
    }
    private String dzwiek;
}