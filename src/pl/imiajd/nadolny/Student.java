package pl.imiajd.nadolny;

public class Student extends Osoba{
    public Student(String nazwisko, int rokurodzenia, String kierunek){
        super(nazwisko, rokurodzenia);
        this.kierunek=kierunek;
    }

    @Override
    public String toString() {
        return "Student{" +
                "nazwisko='" + getNazwisko() + '\'' +
                ", rokurodzenia=" + getRokurodzenia() +
                ", kierunek='" + kierunek + '\'' +
                '}';
    }

    public String getKierunek(){
        return kierunek;
    }
    private String kierunek;
}
