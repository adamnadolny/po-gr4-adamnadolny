package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Student2 extends Osoba2
{
    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis2()
    {
        return String.format("srednia ocen: %.2f ", sredniaOcen);
    }

    public void setOpis2(double a){
        sredniaOcen=a;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    private double sredniaOcen;
    private String kierunek;
}