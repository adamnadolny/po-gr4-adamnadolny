package pl.imiajd.nadolny;

public class Adres{
    public Adres(String ulica, String numer_domu, String numer_mieszkania, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, String numer_domu, String miasto, String kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz(){
        System.out.println(kod_pocztowy+" "+miasto);
        System.out.println(ulica+" "+numer_domu+" "+numer_mieszkania);
    }
    public static boolean przed(Adres a, Adres b) {
        for (int i = 0; i < a.kod_pocztowy.length(); i++) {
            if(i==2){
                i++;
            }
            int s = Integer.parseInt(String.valueOf(a.kod_pocztowy.charAt(i)));
            int g = Integer.parseInt(String.valueOf(b.kod_pocztowy.charAt(i)));
            if (s<g) {
                System.out.println("tak");
                return true;
            }
        }
        System.out.println("nie");
        return false;
    }
    private String ulica;
    private String numer_domu;
    private String numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;
}