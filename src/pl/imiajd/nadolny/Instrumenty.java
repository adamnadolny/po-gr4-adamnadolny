package pl.imiajd.nadolny;

import java.time.LocalDate;
import java.util.Objects;

public abstract class Instrumenty
{
    Instrumenty(String producent, LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }
    public abstract String getDzwiek();

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    @Override
    public String toString() {
        return "Instrumenty{" +
                "Producent='" + getProducent() + '\'' +
                ", Rok Produkcji=" + getRokProdukcji() +
                '}'+"\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrumenty that = (Instrumenty) o;
        return Objects.equals(producent, that.producent) &&
                Objects.equals(rokProdukcji, that.rokProdukcji);
    }

    @Override
    public int hashCode() {
        return Objects.hash(producent, rokProdukcji);
    }

    private String producent;
    private LocalDate rokProdukcji;
}
