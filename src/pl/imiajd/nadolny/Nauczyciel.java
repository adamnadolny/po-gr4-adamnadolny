package pl.imiajd.nadolny;

public class Nauczyciel extends Osoba{
    public Nauczyciel(String nazwisko, int rokurodzenia, double pensja){
        super(nazwisko, rokurodzenia);
        this.pensja=pensja;
    }
    @Override
    public String toString() {
        return "Student{" +
                "nazwisko='" + getNazwisko() + '\'' +
                ", rokurodzenia=" + getRokurodzenia() +
                ", pensja='" + pensja + '\'' +
                '}';
    }
    public double getPensja(){
        return pensja;
    }
    private double pensja;
}