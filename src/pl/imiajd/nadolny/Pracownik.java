package pl.imiajd.nadolny;

import java.time.LocalDate;

public class Pracownik extends Osoba2
{
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, LocalDate dataZatrudnienia, boolean plec, double pobory)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.dataZatrudnienia = dataZatrudnienia;
        this.pobory = pobory;
    }
    public void setOpis2(double a){}
    public String getOpis2()
    {
        return String.format("zatrudniony: %tF ", dataZatrudnienia);
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
    private LocalDate dataZatrudnienia;
    private double pobory;
}
